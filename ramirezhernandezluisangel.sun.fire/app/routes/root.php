<?php

$app->get('/', function() use($app) {
  $datos = array(
    'titulo' => '/',
    'recursos' => array(
//      array('url' => '/api', 'descripcion' => 'Este documento HTML'),
//      array('url' => '/api/xml', 'descripcion' => 'Un documento XML'),
      array('url' => '/api/entidades','descripcion' =>'Entidades'),
      array('url' => '/api/municipio','descripcion' =>'Municipios'),
      array('url' => '/api/tema1','descripcion' =>'Tema1'),
      array('url' => '/api/tema2','descripcion' =>'Tema2'),
      array('url' => '/api/tema3','descripcion' =>'Tema3'),
      array('url' => '/api/indicador','descripcion' =>'Indicador')
    )
  );
  $app->render('root.php', $datos);
});
